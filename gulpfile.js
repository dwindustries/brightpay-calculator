var gulp = require('gulp'),
	rollup = require('gulp-better-rollup'),
	babel = require('rollup-plugin-babel'),
	resolve = require('rollup-plugin-node-resolve'),
	minify = require('gulp-babel-minify'),
	concat = require('gulp-concat'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-ruby-sass'),
	livereload = require('gulp-livereload'),
	imageop = require('gulp-image-optimization'),
	svgSprite = require('gulp-svg-sprite'),
	nunjucksRender = require('gulp-nunjucks-render'),
	data = require('gulp-data'),	
	gutil = require('gulp-util'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload;

// Paths
var assets_path_src = './src/assets';
var assets_path_dist = './public';
var basePaths = {
	src: assets_path_src,
	dest: assets_path_dist,
};
var paths = {
	js: {
		src: basePaths.src+'/js/global.js',
		dest: basePaths.dest+'/js'
	},
	css: {
		src: basePaths.src+'/scss/all.scss',
		dest: basePaths.dest+'/css'
	},
	images: {
		src: basePaths.src + '/images/'
	},
	sprite: {
		src: basePaths.src + '/images/svgs/*',
		svg: basePaths.dest + '/images/svgs/sprite.svg',
		css: basePaths.src + '/scss/_sprite.scss'
	}
};


// Static server
gulp.task('browser-sync', function() {
    //initialize browsersync
    browserSync({
        proxy: "brightpay.l"
    });
});

function errorLog(error) {
	console.error.bind(error);
	this.emit('end');
}


// Render HTML from js
gulp.task('nunjucks', function() {
  return gulp.src('src/pages/**/*.+(njk)')
  .pipe(nunjucksRender({
      path: 'src/templates',
	  envOptions: { watch: true }      
    }))
  .pipe(gulp.dest('public'))
  .pipe(livereload());
});


// Builds and Adds Photoswipe JS Libs
// gulp.task('lib-scripts', function() {
// 	gulp.src([
// 			assets_path_src+'/js/libs/photoswipe.min.js',
// 			assets_path_src+'/js/libs/photoswipe-ui.min.js',
// 			assets_path_src+'/js/libs/twitterFetcher.js',
// 			assets_path_src+'/js/libs/swiper.min.js'
// 		])
// 		.pipe(plumber())
// 		.pipe(concat('lib-scripts.js'))
// 		.pipe(gulp.dest(paths.js.dest))
// 		.pipe(livereload());
// });


// Scripts
// uglifies and concatenates js
gulp.task('scripts', function() {
	gulp.src([
			paths.js.src
		])
	    .pipe(sourcemaps.init())
	    .pipe(rollup({
			plugins: [
					babel({
						exclude: 'node_modules/**'
					}),
					resolve({
						jsnext: true
					})
				]
			}, 'umd')
		)
		.pipe(plumber())
		.pipe(minify({
		mangle: {
			keepClassNames: true
		}
		}))
		.pipe(concat('all.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.js.dest))
		.pipe(livereload());
});


// Styles
// uglifies, uses plumber for errors and live reload
gulp.task('styles', function() {
	return sass(paths.css.src, { 
		style: 'compressed', 
		sourcemap: true 
	})
	.on('error', function (err) {
		console.error('Error', err.message);
	})
    .pipe(sourcemaps.write('maps', {
        includeContent: false,
        sourceRoot: '/source'
    }))
    .pipe(gulp.dest(paths.css.dest))
	.pipe(livereload());
});

// SVG Sprite
// Create an SVG sprite wth png fallbacks
gulp.task('svgSprite', function () {

	return gulp.src(paths.sprite.src)
		.pipe(svgSprite({
			"mode": {
				"css": {
					"spacing": {
						"padding": 5
					},
					"dest": "./",
					"layout": "diagonal",
					"sprite": "../public/images/svgs/sprite.svg",
					"bust": false,
					"render": {
						"scss": {
							"dest": "../src/assets/scss/_sprite.scss",
							"template": assets_path_src+ "/scss/sprite-tpl/sprite-template.scss"
						}
					}
				}
			}
		}))
		.pipe(gulp.dest(basePaths.dest));

});

// Images
// optimise images
gulp.task('images', function(cb) {
    gulp.src([
    	assets_path_src+'/images/**/*.jpg',
    	assets_path_src+'/images/**/*.gif',
    	assets_path_src+'/images/**/*.jpeg',
    	assets_path_src+'/images/**/*.png'
    ])
    .pipe(gulp.dest(basePaths.dest+'/images')).on('end', cb).on('error', cb);
});

// Watch
gulp.task('watch', function() {

	livereload.listen();
	gulp.watch(assets_path_src+'/js/**/*.js',['scripts']);
	gulp.watch(assets_path_src+'/scss/**/*.scss',['styles']);
    gulp.watch('./**/*.njk', ['nunjucks']);

});

// gulp Default
gulp.task('default', ['nunjucks','styles','scripts','svgSprite','images','browser-sync','watch']);