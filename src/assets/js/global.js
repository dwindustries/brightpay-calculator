"use strict";

import Calculator from './components/Calculator.js';

new Vue({
  render: h => h(Calculator),
}).$mount(`#calculator`);
