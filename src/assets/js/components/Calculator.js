// Validation provided by VeeValidate 
Vue.use(VeeValidate, {
  events: 'change' // validate when focus changes (not as you type)
});

export default {
    name: 'Calculator',
    data() {
      return {
        employees: '', 
        payPeriods: '',
        rate: '',
        timePayroll: 0,
        timePayslips: 0.1,
        resultsShown: false,
        annualCost: 0,
        bpTimePayroll: 5,
        bpCostOfTime: 0,
        bpSoftwareCost: 149,
        bpSaving: 0,
        tooltipPayrollShown: false,
        tooltipPayslipsShown: false
      }
    },
    methods: {
      handleResults: function() {
        this.$validator.validateAll().then(() => {
            if (!this.errors.any()) {
                this.showResults();
                this.handleTimePayroll();
                this.handleTimePayslips();
                this.handleAnnualCost();
                this.handleTimeCost();
                this.handleSaving();
                const results = document.getElementById('calc-results');
                setTimeout(() => {
                results.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
                }, 5);
            } else {
                this.resetCalculator();
            }
        });
      },      
      showResults: function(){
        this.resultsShown = true; 
      },   
      handleTimePayroll: function() {
        this.timePayroll = Number(this.employees * 0.2);
      },
      handleTimePayslips: function() {
        this.timePayslips = Number(this.employees * 0.1);
      },
      handleAnnualCost: function() {
        this.annualCost = Number((this.timePayroll + this.timePayslips) * this.payPeriods * this.rate);
      },
      handleTimeCost: function() {
        this.bpCostOfTime = Number(this.payPeriods * this.rate / 12); // divided by 12 months
      },
      handleSaving: function() {
        if(this.employees < 2) { this.bpSoftwareCost = 0; }
        this.bpSaving = Number(this.annualCost - this.bpCostOfTime - this.bpSoftwareCost);
      },
      formatMoney(value) {
        let price = value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        return price;
      },
      formatHours(value) {
        // Convert the time val in to hours and mins string
        let hours,minutes,time = 0;
        let hoursString,minsString = '';

        // Convert to hours int
        hours = value.toFixed(2) * 1;
        hoursString = (hours < 2) ? 'hr' : 'hrs';
        hoursString = parseInt(hours).toString() + hoursString;
        hoursString = (hours < 1) ? '' : hoursString;

        // Convert to mins
        minutes = (hours * 60) % 60;
        minsString = (minutes == 0) ? '' : minutes.toString() + 'mins';

        // Output result as time
        time = hoursString + ' ' + minsString;
        return time;
      },
      resetCalculator() {
        this.resultsShown = false; 
        this.employees = ''; 
        this.payPeriods = '';
        this.rate = '';
        const calc = document.getElementById('calc-form');
        setTimeout(() => {
          calc.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
        }, 5);
      },
      manageTooltip(el) {
        if(el == 1) {
          this.tooltipPayslipsShown = false;
          this.tooltipPayrollShown = true;
        } else {
          this.tooltipPayrollShown = false;
          this.tooltipPayslipsShown = true;
        }
      },
      closeTooltip(el) {
        this.tooltipPayrollShown = false;
        this.tooltipPayslipsShown = false;
      }     
    },
    template: `
      <div class="calculator">
        <section id="calc-form" class="calc-section calc-section--bg">
          <div class="calc-container">
            <h2 class="calc__title">If you are calculating your payroll manually, try our savings calculator to learn how much you could save with BrightPay&hellip;</h2>
            <form class="calc" v-on:submit.prevent="onSubmit">
                <div class="calc__item">
                    <label class="calc__label calc__label--light" for="employees">How many employees do you have?</label>
                    <input class="calc__input" name="employees" v-model.number="employees" v-validate="{ required: true, min_value: 1 }" type="number" aria-required="false" aria-invalid="true">
                    <span class="calc__error">{{ errors.first('employees') }}</span>
                </div>
                <div class="calc__item">
                  <label class="calc__label calc__label--light" for="payPeriods">How many pay periods do you have in a year? <span class="calc__label__note">(Weekly = 52 / Monthly = 12)</span></label>
                  <input class="calc__input" name="payPeriods" v-model.number="payPeriods" type="number" v-validate="{ required: true, min_value: 1, max_value: 52 }"  />
                  <span class="calc__error">{{ errors.first('payPeriods') }}</span>
                </div>
                <div class="calc__item">
                  <label class="calc__label calc__label--light" for="rate">How much is your time worth per hour?</label>
                  <div class="calc__input-container">
                    <input class="calc__input" name="rate" v-model.number="rate" type="number" v-validate="{ required: true, min_value: 1 }" />
                  </div>
                  <span class="calc__error">{{ errors.first('rate') }}</span>
                </div>
                <button class="calc__button" @click.prevent="handleResults" type="submit">Calculate Saving</button>
            </form>
          </div>
        </section>
        <section id="calc-results" class="calc-section calc-section--white" v-show="resultsShown">
          <div class="calc-container">
            <div class="calc__results">
              <h2 class="calc__result-title">Cost to Manually Process</h2>
              <div class="calc__item calc__item--result">
                <p class="calc__label">Time you will spend doing payroll manually each pay period</p>
                <p class="calc__result-val">{{ formatHours(timePayroll) }} <span class="calc__tooltip" @click="manageTooltip(1)"><i class="icon icon--info"></i></span></p>
                <div class="tooltip" v-show="tooltipPayrollShown">
                  <p>This includes time on calculations, dealing with joiners/leavers, handling corrections and inputting all details on ROS screens. Also, includes time to prepare reports for accountants.</p>
                  <span class="tooltip__close" @click="closeTooltip"><i class="icon icon icon--x"></i></span>
                </div>
              </div>
              <div class="calc__item calc__item--result">
                <p class="calc__label">Time you will spend creating and distributing payslips for your employees</p>
                <p class="calc__result-val">{{ formatHours(timePayslips) }} <span class="calc__tooltip" @click="manageTooltip(2)"><i class="icon icon--info"></i></span></p>
                <div class="tooltip" v-show="tooltipPayslipsShown">
                  <p>This includes time on calculations, dealing with joiners/leavers, handling corrections and inputting all details on ROS screens. Also, includes time to prepare reports for accountants.</p>
                  <span class="tooltip__close" @click="closeTooltip"><i class="icon icon icon--x"></i></span>
                </div>
              </div>
              <div class="calc__item calc__item--result">
                <p class="calc__label">Total annual cost doing it manually</p>
                <p class="calc__result-val">&euro;{{ formatMoney(annualCost) }}</p>
              </div>
              <div class="calc__block">
                <h3 class="calc__result-title calc__result-title--light">Cost to process using BrightPay</h3>
                <div class="calc__item calc__item--result calc__item--block">
                  <p class="calc__label calc__label--light">Time to process payroll (including payslips)</p>
                  <p class="calc__result-val calc__result-val--dark">{{ bpTimePayroll }} Minutes</p>
                </div>
                <div class="calc__item calc__item--result calc__item--block">
                  <p class="calc__label calc__label--light">Cost of your time</p>
                  <p class="calc__result-val calc__result-val--dark">&euro;{{ formatMoney(bpCostOfTime) }}</p>
                </div>
                <div class="calc__item calc__item--result calc__item--block">
                  <p class="calc__label calc__label--light">Software cost</p>
                  <p class="calc__result-val calc__result-val--dark">&euro;{{ formatMoney(bpSoftwareCost) }}</p>
                </div>
                <div class="calc__item calc__item--result calc__item--saving">
                  <p class="calc__label calc__label--saving">Cost Saving per annum</p>
                  <p class="calc__result-val calc__result-val--saving">&euro;{{ formatMoney(bpSaving) }}</p>
                </div>
                <footer class="calc__footer">
                  <p class="calc__vat">Plus Vat / Tax Year</p>
                  <a class="calc__reset" @click.prevent="resetCalculator"><span>Reset Calculator</span> <i class="icon icon--reset"></i></a>
                </footer>
              </div>
            </div>
          </div>
        </section>
      </div>
    `,
  }