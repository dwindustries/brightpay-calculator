# BrightPay Calculator

Simple pure JS calculator app, made to be dropped into the BrightPay website.

See gulpfile.js and package.json for more details.

Generated with gulp & Nunjucks, currently using Browser sync with MAMP Pro as Server,
but could easily be served elsewhere locally.

I have had to apply a Polyfill for IE11 to understand ES6 Promises.
Annoyingly couldn't get this working with babel & rollup - possibly not transpiling third-party npm modules.